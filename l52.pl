package coolstuff;

BEGIN {
    use Exporter();
    @ISA= qw(Exporter);
    @EXPORT= qw(&coolfunkshun $scalar);
}

sub coolfunkshun {print "a" x 16 .v10;}

AUTOLOAD {print "No such funkshun ’$AUTOLOAD’\n"};

return 1;
END {}
