#!/usr/bin/perl -w

$a= 2;
$b= 1;
$pi= 1;
$e= 500000000;
$loop_counter= 1;

while($a < $e)
  {
    #Сокращённая запись. То же самое, что $pi= $pi*($a/$b);
    $pi*= $a / $b;
    $b+= 2;
    $pi*= $a / $b;
    $a+= 2;
    $loop_counter++;
  }

$pi*= 2;

print "Количество итераций: $loop_counter\n"
    . "Получено число пи: $pi\n";
