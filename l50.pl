package coolstuff;

BEGIN {print "Entering " . __PACKAGE__ . "\n";}

sub coolfunkshun {print "a" x 16 .v10;}

AUTOLOAD {print "No such funkshun '$AUTOLOAD'\n"};
return 1;
END {print "Exiting ". __PACKAGE__ . "\n";}
