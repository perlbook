#!/usr/bin/perl -w
use strict;
use warnings;
use utf8;
use v5.10;

my %EOT= (
    Имя   => 'Рита',
    Глаза => 'Голубые',
    Вес   => '49кг',
    Рост  => '1.76м'
);

my %address= (
    Город   => 'Усть-Качка',
    Улица   => 'Партизана Германа',
    Дом     => '76',
    Квартира=> '15',
    Телефон => '9(943)345-228-0'
);

my @data_base= (\%EOT, \%adress);
say $data_base[0]{Имя};
