#!/usr/bin/perl -w

use strict;
use warnings;
use v5.10;

my $scalar= 12345;
my $ref= \$scalar;

say $ref;
++$$ref; #можно было записать так: ++${ $ref };
say $scalar;
