#!/usr/bin/perl -w

use strict;
use warnings;
use v5.10;

sub average
  {
    return 0  unless @_;
    my $sum= 0;
    map {$sum+= $_} @_;
    $sum/= @_;
  }


say "Среднее значение: " . average(1,2,3,4);
