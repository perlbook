#!/usr/bin/perl -w

use strict;
use warnings;
use v5.10;

sub merge ($$)
  {
    my ($first, $second)= @_;
    @ {$first}= sort (@ {$first}, @ {$second});
  }

my @a= qw(corge grault garply);
my @b= qw(foo bar baz quux);
merge(\@a, \@b);
map {print "$_ "} @a;
