#!/usr/bin/perl
use warnings;
use strict;

use constant
  {
    SPEED_OF_LIGHT => 299_792_458,
    AVERAGE_DISTANCE_TO_MOON => 384400000,
  };

print "Свет от земли до луны дойдет за " .
AVERAGE_DISTANCE_TO_MOON/SPEED_OF_LIGHT . "сек\n";
