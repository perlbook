#!/usr/bin/perl -w
use strict;
use warnings;
my $text= 'foo bar 666 42 baz quux 124';

while ($text =~ /\b([^a-z ]+?)\b/g) {
    if ($1 eq "") {next;}
    print "$1\n";
}
