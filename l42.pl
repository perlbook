#!/usr/bin/perl -w
use strict;
use warnings;
my $text= 'Variable names in Perl can have several formats. Usually,
they must begin with a letter or underscore, in which case they
can be arbitrarily long (up to an internal limit of 251 characters)
and may contain letters, digits, underscores, or the special
sequence :: or \’. In this case, the part before the last :: or \’
is taken to be a package qualifier; see perlmod.';

while ($text =~ /\b(\w{5,})\b/g)
  {
    print "$1\n";
  }
