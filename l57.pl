#!/usr/bin/perl

use strict;
use warnings;

our $scalar= "Глобально и надёжно!";

sub tell_the_truth
  {
    local $scalar= $scalar;
    chop $scalar;
    $scalar= $scalar . "- разные понятия!";
  }

print tell_the_truth() .v10;

print "$scalar\n";
