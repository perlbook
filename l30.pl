#!/usr/bin/perl -w

use strict;
use warnings;
use v5.10;

my @arr= qw(foo bar baz fobar qux quux);
my $aref= \@arr;
say $$aref[1];
say ${ $aref}[2];
say $aref->[3];
