package mycounter;

BEGIN {
    use Exporter();
    @ISA= qw(Exporter);
    @EXPORT= qw(&inc &dec &show);
}

our $counter= 0;
sub inc {$counter++;}
sub dec {$counter--;}
sub show {print "$counter\n";}

AUTOLOAD {print "No such funkshun '$AUTOLOAD'\n"};

return 1;
END {}
